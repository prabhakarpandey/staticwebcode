package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testadd() throws Exception {

        int k= new Calculator().add();
        assertEquals("add", 9, k);
        
    }
   
    @Test
    public void testsub() throws Exception {

        int k= new Calculator().sub();
        assertEquals("sub", 3, k);

    }
}
